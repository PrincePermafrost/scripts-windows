$name = Read-Host "Enter the name of the service (upnphost/SSDPSRV)"
""
$type = Read-Host "Enter the Startup Type (Automatic, Manual, Disabled)"

Set-Service -Name $name -StartupType $type -Status Running

""
Write-Host "Service $name up and running." -ForegroundColor Green
""

Start-Sleep -s 2