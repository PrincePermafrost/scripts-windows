"Loading the current Languages list..."

Get-WinUserLanguageList

"Examples of Languages: fr-FR | fr-CA | en-US | en-CA"

$input = Read-Host "Please choose a language"

Set-WinUserLanguageList $input -Force -Confirm

Write-Host "Language changed. Somes Updates might be required." -ForegroundColor Green

Start-Sleep -s 2