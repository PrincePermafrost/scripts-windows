function Disable-InternetExplorerESC {
    $AdminKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}"
    $UserKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}"
    Set-ItemProperty -Path $AdminKey -Name "IsInstalled" -Value 0
    Set-ItemProperty -Path $UserKey -Name "IsInstalled" -Value 0
    Stop-Process -Name Explorer
    
    ""
    Write-Host "IE Enhanced Security Configuration (ESC) disabled." -ForegroundColor Green
    ""
    Start-Sleep -s 2
}

function Disable-InternetExplorerESC_Admins {
    $AdminKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}"
    #$UserKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}"
    Set-ItemProperty -Path $AdminKey -Name "IsInstalled" -Value 0
    #Set-ItemProperty -Path $UserKey -Name "IsInstalled" -Value 0
    Stop-Process -Name Explorer
    
    ""
    Write-Host "IE Enhanced Security Configuration (ESC) disabled." -ForegroundColor Green
    ""
    Start-Sleep -s 2
}

function Disable-UserAccessControl {
    Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "ConsentPromptBehaviorAdmin" -Value 00000000
    
    ""
    Write-Host "User Access Control (UAC) disabled." -ForegroundColor Green
    ""
    Start-Sleep -s 2
}

$input5 = Read-Host $left$clock $user$right "Would you like to disable IE Enhanced Security Configuration and User Access Control only for Admins ? (Y/N)"
     switch ($input5)
     {
             'y' {Disable-InternetExplorerESC_Admins}
             'n' {Disable-InternetExplorerESC}
     }

Disable-UserAccessControl

Start-Sleep -s 1