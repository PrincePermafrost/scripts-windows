function Show-Menu
    {
         param (
               [string]$Title = 'Firewall'
         )
         cls
         ""
         Write-Host "================ $Title ================"
         ""
         Write-Host "1: Public"
         Write-Host "2: Private"     
         Write-Host "3: Domain"
         Write-Host "4: All"         
         ""
         Write-Host "Q: Quit." -ForegroundColor Red
    }

    do
{
     Show-Menu
    ""
$input4 = Read-Host "Which Firewall do you want to disable ?"
     switch ($input4)
     {
             '1'{Set-NetFirewallProfile -Name Public -Enabled False
                cls
                ""
                Write-Host "Done" -ForegroundColor Green
            }
             '2'{Set-NetFirewallProfile -Name Private -Enabled False
                ""
                cls
                Write-Host "Done" -ForegroundColor Green
            }
             '3'{Set-NetFirewallProfile -Name Domain -Enabled False
                ""
                cls
                Write-Host "Done" -ForegroundColor Green
            }
             '4'{Set-NetFirewallProfile -Name Public,Private,Domain -Enabled False
                ""
                cls
                Write-Host "Done" -ForegroundColor Green
            }
             'q' {return}
            }
            pause
}
until ($input4 -eq 'q')