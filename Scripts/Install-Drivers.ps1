"Installing Virtio Drivers."
""

Start-Sleep -s 1

"Please insert the Virtio-Win ISO"
""

Read-Host -Prompt "Once the ISO has been inserted, press a key to continue..."

Get-ChildItem "D:\*\2k16\amd64" -Recurse -Filter "*.inf" |
ForEach-Object { PNPUtil.exe /add-driver $_.FullName /install }

""
Write-Host "Drivers Installed." -ForegroundColor Green
""

Start-Sleep -s 2
