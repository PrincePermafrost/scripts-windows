$input = Read-Host "Please choose a new name for the Computer"

Rename-Computer -NewName $input -DomainCredential WORKGROUP\Administrator

Write-Host "Computer name changed." -ForegroundColor Green

""

Start-Sleep -s 2