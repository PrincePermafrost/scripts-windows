
$domgroup = Read-Host "Would you like to join a Domain or a Workgroup ? (D/W)"
                switch ($domgroup)
                {
                      'd' {"" 
                      
                      Write-Host "Please make sure the Computer is not currently a member of an existing Domain before beginning." -ForegroundColor Red
                      ""
                      Read-Host -p "Press any key to continue"
                      ""
                      $domain = Read-Host "Please enter the name of the Domain you want to join"

                      $domgroup = Read-Host "Do you need to change your DNS server ? (Y/N)"
                      switch ($domgroup)
                      {
                      'y' {Invoke-Item -Path "C:\Windows\System32\ncpa.cpl"
                        Add-Computer -DomainName $domain
                      
                        Write-Host "Domain joined." -ForegroundColor Green
                      
                        Start-Sleep -s 2 }
                      'n' {Add-Computer -DomainName $domain
                      
                        Write-Host "Domain joined." -ForegroundColor Green
                        
                        Start-Sleep -s 2 }
                      }

                      ""}
                      
                      'w' {"" 
                      
                      $workgroup = Read-Host "Please enter the name of the Workgroup you want to join" 
                      
                      Add-Computer -WorkGroupName $workgroup
                    
                      Write-Host "Workgroup joined." -ForegroundColor Green
                    
                    Start-Sleep -s 2 
                    
                    ""}
                }