"Extracting Powershell Module for Windows Update..."
""

Start-Sleep -s 1


Expand-Archive -Force $pwd\Scripts\PSWindowsUpdate.zip -DestinationPath C:\

""
Write-Host "Powershell Module for Windows Update extracted to the system root." -ForegroundColor Green
""

Start-Sleep -s 2

"Please move the folder C:\PSWindowsUpdate to C:\Windows\System32\WindowsPowerShell\v1.0\Modules\"
""

Read-Host -Prompt "Once the folder has been moved, press any key to continue..."

$modulelocation = "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\PSWindowsUpdate"

if (Test-Path $modulelocation) {Write-Host "Folder correctly moved." -ForegroundColor Green}
else {Write-Host "Folder not found." -ForegroundColor Red
return
}


Start-Sleep -s 2

PowerShell.exe -noexit -ExecutionPolicy Bypass -File .\Scripts\Upgrade.ps1
