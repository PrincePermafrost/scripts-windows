"Disabling Server Manager automatic startup at logon."
""

Start-Sleep -s 1

REG.exe Query HKCU\Software\Microsoft\ServerManager /V DoNotOpenServerManagerAtLogon

REG.exe Add HKCU\Software\Microsoft\ServerManager /V DoNotOpenServerManagerAtLogon /t REG_DWORD /D 0x1 /F

Get-ScheduledTask -TaskName ServerManager | Disable-ScheduledTask -Verbose
""
Write-Host "Server Manager automatic startup disabled." -ForegroundColor Green
""

Start-Sleep -s 2