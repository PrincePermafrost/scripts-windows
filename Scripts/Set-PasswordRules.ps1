$input3 = Read-Host "Would you like to disabke Password Complexity Requirements ? (Y/N)"
     switch ($input3)
     {
             'y'{
                 Get-ADDefaultDomainPasswordPolicy -Current LoggedOnUser | Set-ADDefaultDomainPasswordPolicy -ComplexityEnabled $false  -MinPasswordLength 0
                 secedit /export /cfg c:\secpol.cfg
                 (gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
                 secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg /areas SECURITYPOLICY
                 rm -force c:\secpol.cfg -confirm:$false
                 
                 cls
                 Write-Host "Password Complexity Requirements disabled." -ForegroundColor Green
                }
             'n' {}
     }

""

Start-Sleep -s 1




""

Start-Sleep -s 2

$user = Read-Host "Please enter the username of the user you want to change the password of"
$password =  Read-Host "Please enter the new password"

"Setting password."
""

net user $user $password

""
Write-Host "$user password set to $password ." -ForegroundColor Green
""

Start-Sleep -s 2