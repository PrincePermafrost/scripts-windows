function Show-Menu
    {
         param (
               [string]$Title = 'Post-Installation scripts'
         )
         cls
         Write-Host "================ $Title ================"
         ""
         Write-Host "1: Rename the Computer."
         Write-Host "2: Join a Domain/Workgroup."
         Write-Host "3: Set the Region."
         Write-Host "4: Set the Language."
         Write-Host "5: Disable Server Manager at Logon."
         Write-Host "6: Disable IE ESC and UAC."
         Write-Host "7: Set the Passwords and Disable the Complexity Requirements."
         Write-Host "8: Install the VirtIO Drivers."
         Write-Host "9: Enable and Start Services."
         Write-Host "A: Disable the Firewall."
         Write-Host "B: Download Updates."         
         ""
         Write-Host "Q: Return to the previous menu." -ForegroundColor Red
    }

    do
{
     Show-Menu
     ""
     $left = "["
     $right = "]"
     $money = "$"
     $user = whoami
     $clock = "{0:HH:mm}" -f (get-date)
     $input = Read-Host $left$clock $user$right "Enter command number/letter"
     switch ($input)
     {
             '1' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Rename-Computer.ps1
           } '2' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Join-Domain.ps1
           } '3' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Set-Region.ps1
           } '4' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Set-Language.ps1
           } '5' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Disable-ServerManagerAtLogon.ps1
           } '6' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Disable-InternetExplorerESC.ps1
           } '7' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Set-PasswordRules.ps1
           } '8' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Install-Drivers.ps1
           } '9' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Start-Services.ps1
           } 'a' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Disable-Firewall.ps1
           } 'b' {
                cls
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Install-Update.ps1
           } 'q' {
                return
           }
     }
     pause
}
until ($input -eq 'q')