function Get-RegionInfo($Name='*')
{
    $cultures = [System.Globalization.CultureInfo]::GetCultures('InstalledWin32Cultures')
 
    foreach($culture in $cultures)
    {
       try{
           $region = [System.Globalization.RegionInfo]$culture.Name
 
           if($region.DisplayName -like $Name)
           {
                $region
           }
       }
       catch {}
    }
}

Get-RegionInfo | Sort-Object DisplayName | Select-Object Name,DisplayName,GeoId | Out-GridView
""
$input = Read-Host "Please enter the number corresponding to your country"

Set-WinHomeLocation $input
""
Write-Host "Country changed." -ForegroundColor Green

Start-Sleep -s 2
""
"Examples of Regions: fr-FR | fr-CA | en-US | en-CA"
""
$input = Read-Host "Please enter your Region"

Set-Culture $input
""
Write-Host "Region changed." -ForegroundColor Green

Start-Sleep -s 2