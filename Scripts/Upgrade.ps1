"Importing module PSWindowsUpdate..."
""
Start-Sleep -s 1

Import-Module PSWindowsUpdate

"Listing updates already installed on the local computer..."
""
Get-Hotfix

Start-Sleep -s 6

#Add-WUServiceManager -ServiceID 7971f918-a847-4430-9279-4a52d1efe18d
""
"Downloading new updates..."
""

Start-Sleep -s 1

Get-WUInstall -NotCategory "Language packs" -IgnoreUserInput -WindowsUpdate -DownloadOnly -AcceptAll -IgnoreReboot -Verbose

""
Write-Host "Updates downloaded. Reboot required to finish installation." -ForegroundColor Green
""

Start-Sleep -s 10
