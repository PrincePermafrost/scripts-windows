function Show-Menu
    {
         param (
               [string]$Title = 'Get Infos about...'
         )
         cls
         Write-Host "================ $Title ================"
         ""
         Write-Host "1: Bios."
         Write-Host "2: Computer System."
         Write-Host "3: Product."
         Write-Host "4: Computer System Product."
         Write-Host "5: Processor."
         Write-Host "6: Disk Drive."
         Write-Host "7: Share."
         Write-Host "8: Operating System."
         Write-Host "9: CDRom Drive."
         Write-Host "A: Battery."
         Write-Host "B: Network Adapter."
         Write-Host "C: Physical Memory."
         Write-Host "D: Services."    
         ""
         Write-Host "Q: Return to the previous menu" -ForegroundColor Red
    }


    do
{
     Show-Menu
     ""
     $left = "["
     $right = "]"
     $money = "$"
     $user = whoami
     $clock = "{0:HH:mm}" -f (get-date)
     $input = Read-Host $left$clock $user$right "Enter command number/letter"
     switch ($input)
     {
             '1' {
                cls
                $question1 = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($question1)
                {
                      'y' {Get-WmiObject -Class Win32_Bios | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_Bios}
                }
           } '2' {
                cls
                $question2 = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($question2)
                {
                      'y' {Get-WmiObject -Class Win32_ComputerSystem | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_ComputerSystem}
                }
           } '3' {
                cls
                $question3 = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($question3)
                {
                      'y' {Get-WmiObject -Class Win32_Product | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_Product}
                }
           } '4' {
                cls
                $question4 = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($question4)
                {
                      'y' {Get-WmiObject -Class Win32_ComputerSystemProduct | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_ComputerSystemProduct}
                }
           } '5' {
                cls
                $question5 = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($question5)
                {
                      'y' {Get-WmiObject -Class Win32_Processor | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_Processor}
                }
           } '6' {
                cls
                $question6 = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($question6)
                {
                      'y' {Get-WmiObject -Class Win32_DiskDrive | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_DiskDrive}
                }
           } '7' {
                cls
                $question7 = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($question7)
                {
                      'y' {Get-WmiObject -Class Win32_Share | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_Share}
                }
           } '8' {
                 cls
                $question8 = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($question8)
                {
                      'y' {Get-WmiObject -Class Win32_OperatingSystem | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_OperatingSystem}
                }
           } '9' {
                 cls
                $question9 = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($question9)
                {
                      'y' {Get-WmiObject -Class Win32_CDRomDrive | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_CDRomDrive}
                }
                
           } 'a' {
                cls
                $questionA = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($questionA)
                {
                      'y' {Get-WmiObject -Class Win32_Battery | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_Battery}
                }
           } 'b' {
                cls
                $questionB = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($questionB)
                {
                      'y' {Get-WmiObject -Class Win32_NetworkAdapter | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_NetworkAdapter}
                }
           } 'c' {
                cls
                $questionC = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($questionC)
                {
                      'y' {Get-WmiObject -Class Win32_PhysicalMemory | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_PhysicalMemory}
                }
           } 'd' {
                cls
                $questionD = Read-Host "Do you want detailed info ? (Y/N)"
                switch ($questionD)
                {
                      'y' {Get-WmiObject -Class Win32_Service | Format-List * | More}
                      'n' {Get-WmiObject -Class Win32_Service}
                }
           } 'q' {
                return
           }
     }
     pause
}
until ($input -eq 'q')