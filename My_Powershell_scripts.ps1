function Show-Menu
    {
         param (
               [string]$Title = 'My Powershell scripts'
         )
         cls
         ""
         Write-Host "================ $Title ================"
         ""
         Write-Host "L: List of my Post-Installation scripts."
         Write-Host "I: Get information about this computer."     
         Write-Host "S: Search for a file."
         ""
         Write-Host "Q: Quit." -ForegroundColor Red
    }

    do
{
     Show-Menu
     ""
     $left = "["
     $right = "]"
     $money = "$"
     $user = whoami
     $clock = "{0:HH:mm}" -f (get-date)
     $input = Read-Host $left$clock $user$right "Enter command letter"
     switch ($input)
     {
             'L' {
                cls
                'Loading... Please wait.'
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (1/4)
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (1/2)
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (2/3)
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (1/1)
                ""
                Write-Host 'Done !' -ForegroundColor Green
                Start-Sleep -s 1
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Menu-Post-Installation.ps1
           } 'i' {
                cls
                'Loading... Please wait.'
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (1/4)
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (1/2)
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (2/3)
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (1/1)
                ""
                Write-Host 'Done !' -ForegroundColor Green
                Start-Sleep -s 1
                PowerShell.exe -ExecutionPolicy Bypass -File .\Scripts\Menu-Get-Infos.ps1
           } 's' {
                cls
                "Listing existing Drives..."
                Start-Sleep -s 1
                cls  
                [System.IO.DriveInfo]::GetDrives() | Format-Table
                ""
                Start-Sleep -s 1

                        Add-Type -AssemblyName System.Windows.Forms
                        Add-Type -AssemblyName System.Drawing

                        $form = New-Object System.Windows.Forms.Form
                        $form.Text = 'Search'
                        $form.Size = New-Object System.Drawing.Size(300,200)
                        $form.StartPosition = 'CenterScreen'

                        $OKButton = New-Object System.Windows.Forms.Button
                        $OKButton.Location = New-Object System.Drawing.Point(112.5,120)
                        $OKButton.Size = New-Object System.Drawing.Size(75,23)
                        $OKButton.Text = 'OK'
                        $OKButton.DialogResult = [System.Windows.Forms.DialogResult]::OK
                        $form.AcceptButton = $OKButton
                        $form.Controls.Add($OKButton)

                        $label = New-Object System.Windows.Forms.Label
                        $label.Location = New-Object System.Drawing.Point(10,20)
                        $label.Size = New-Object System.Drawing.Size(280,20)
                        $label.Text = 'Enter the Path from where you want to start the search:'
                        $form.Controls.Add($label)

                        $textBox = New-Object System.Windows.Forms.TextBox
                        $textBox.Text = "C:\"
                        $textBox.Location = New-Object System.Drawing.Point(10,40)
                        $textBox.Size = New-Object System.Drawing.Size(260,20)
                        $form.Controls.Add($textBox)

                        $form.Topmost = $true

                        $form.Add_Shown({$textBox.Select()})
                        $result = $form.ShowDialog()

                        if ($result -eq [System.Windows.Forms.DialogResult]::OK)
                        {
                              $pathvalue = $textBox.Text
                        }

                $filename = Read-Host $left$clock $user$right "Enter the Name of your file"

                 cls
                'Loading... Please wait.'
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (1/4)
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (1/2)
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (2/3)
                Start-Sleep -s 1
                cls
                'Loading... Please wait.'
                ""
                "{0:p0}" -f (3/4)
                ""        
                $input1 = Read-Host $left$clock $user$right "Would you like to open the file(s) after the search ? (Y/N)"
                switch ($input1)
                {
                'y' {Get-ChildItem -Path $pathvalue -Recurse | Where-Object {$_.Name -match $filename} | Invoke-Item}
                'n' {Get-ChildItem -Path $pathvalue -Filter $filename -Recurse}
                }
                
                ""  
                "{0:p0}" -f (1/1)
                ""
                Write-Host 'Done !' -ForegroundColor Green
                ""
                Start-Sleep -s 1
                
           } 'q' {
                return
           }
     }
     pause
}
until ($input -eq 'q')